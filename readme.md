## About API

A aplicação é um gerenciador de catálogo de produto onde a atualização e criação de produtos é feita através de uma planilha Excel

O que vamos avaliar principalmente no seu teste:
- Design do código
- Testes automatizados
- Padrão de código
- Documentação
- Pontualidade
- Comprometimento

## Objetivo do produto a ser entregue:
● Criar uma API RESTful que:
  - Receberá uma planilha de produtos (segue em anexo) que deve ser processada em background (queue)
  - Ter um endpoint que informe se a planilha for processada com sucesso ou não
  - Seja possível visualizar, atualizar e apagar os produtos (só é possível criar novos produtos via planilha)

## O que vamos avaliar no teste:
● Se você atingiu o objetivo do produto a ser desenvolvido
● Identificar seu conhecimento em testes automatizados (pirâmide de testes)
● Avaliar seu conhecimento sobre APIs
● Avaliar a arquitetura e organização do código criado
● Se a sua solução foi simples de entender na visão de outro desenvolvedor

## Requisitos técnicos
● Laravel
● Git/Gitlab
● REST
● JSON
● Utilizar Queue
● Testes automatizados (PHPUnit + Mockery)
