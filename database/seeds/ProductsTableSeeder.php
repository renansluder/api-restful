<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('products')->insert([
            'lm'            => 1001,
            'name'          => 'Furadeira X',
            'category'      => 12123,
            'free_shipping' => 0,
            'description'   => 'Furadeira eficiente X',
            'price'         => '100.00',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);
	}
}
