<?php

namespace App\Jobs;

use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;

class ExcelToDatabase implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    private $urlExcel;

    /**
     * Create a new job instance.
     *
     * @param string $urlExcel
     */
    public function __construct(string $urlExcel)
    {
        $this->urlExcel = $urlExcel;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Reading excel and converting to array
        $data = Excel::load($this->urlExcel, function($results) {
            $results->all();
            $results->limitColumns(5);
            $results->limitRows(10);
            $results->noHeading();
        })->get()->toArray();

        // The generated excel structure, each activity was separated by line
        $category = '';
        foreach ($data[0] as $key => $value){

            // First line
            if ($key===0) {

                // Set category
                $category = ($value[1] != 'category' ? $value[1] : null);

            } elseif ($key>=3){
                // Third line

                // Verifying the product to be registered if it already exists in the database and deleting it
                Product::where(['lm' => $value[0], 'category' => $category])->delete();

                // Insert data into the database
                $Product = new Product;
                $Product->fill(
                    array(
                        'lm' => $value[0],
                        'category' => $category,
                        'name' => $value[1],
                        'free_shipping' => $value[2],
                        'description' => $value[3],
                        'price' => $value[4]
                    )
                );
                $Product->save();
            }
        }
    }
}