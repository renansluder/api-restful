<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	use softDeletes;

	protected $fillable = ['lm', 'category', 'name', 'free_shipping', 'description', 'price'];

	protected $dates = ['deleted_at'];
}