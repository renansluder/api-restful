<?php

namespace App\Http\Controllers;

use App\Product;
use App\Jobs\ExcelToDatabase;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $Product = Product::all();

            return response()->json($Product, 200);

        } catch (MethodNotAllowedHttpException $e) {
            return response()->json(['message' => 'Requisition denied'], 404);

        } catch (QueryException $e) {
            return response()->json(['message' => 'Database is unavailable, try again later'], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            if (!$request->hasFile('file')) {
                return response()->json(['message' => 'File not found'], 404);
            }

            $patch = $request->file('file')->move(
                public_path('uploads/excel/'),
                date('YmdHis').'.'.$request->file('file')->extension()
            );

            $queue_id = $this->dispatch((new ExcelToDatabase($patch))->onQueue('excel'));
            if ($queue_id <= 0) {

                return response()->json(['message' => 'Error processing excel'], 500);
            }

            // Stops for 5 seconds for the queue to process
            sleep(5);

            // Checks if Queue has not been processed
            $return = DB::table('jobs')->where('id', $queue_id)->exists();
            if ($return) {

                // Process performed without Queue execution
                return response()->json(['message' => 'Process carried out without data loading'], 202);
            }

            // Checking queue for processing failed
            $return = DB::table('failed_jobs')->where('queue', 'excel')->doesntExist();
            if ($return) {

                return response()->json(['message' => 'Products entered'], 201);
            }

            // Process carried out with error
            return response()->json(['message' => 'Error processing excel'], 500);

        } catch (MethodNotAllowedHttpException $e) {
            return response()->json(['message' => 'Requisition denied'], 404);

        } catch (QueryException $e) {
            return response()->json(['message' => 'Database is unavailable, try again later'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $Product)
    {
        try {

            if (!$Product) {
                return response()->json(['message' => 'Record not Found']);
            }

            return response()->json($Product, 200);

        } catch (MethodNotAllowedHttpException $e) {
            return response()->json(['message' => 'Requisition denied'], 404);

        } catch (QueryException $e) {
            return response()->json(['message' => 'Database is unavailable, try again later.'], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $Product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product_id)
    {
        try {

            // Picking up and verifying the product to be updated
            $Product = Product::find($product_id);
            if (!$Product) {
                return response()->json(['message' => 'Record not found'], 404);
            }

            $Product->fill($request->all());
            $Product->save();

            return response()->json($Product, 201);

        } catch (MethodNotAllowedHttpException $e) {
            return response()->json(['message' => 'Requisition denied'], 404);

        } catch (QueryException $e) {
            return response()->json(['message' => 'Database is unavailable, try again later.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $Product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $Product)
    {
        try {

            if (!$Product) {
                return response()->json(['message' => 'Record not found'], 404);
            }

            $Product->delete();

            return response()->json(['message' => 'Register Deleted'], 200);

        } catch (MethodNotAllowedHttpException $e) {
            return response()->json(['message' => 'Requisition denied'], 404);

        } catch (QueryException $e) {
            return response()->json(['message' => 'Database is unavailable, try again later.'], 500);
        }
    }
}