<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * Get Product
     *
     * @return int
     */
    public function getProductOk()
    {
        $product_id = DB::table('products')
            ->select('id')
            ->where('deleted_at', NULL)
            ->first();
        $product_id = (array)$product_id;

        return (int)$product_id['id'];
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testResponseHomeOk()
    {
        $response = $this->get('http://127.0.0.1:8000/');
        $response->assertOk();
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testResponseHomeError()
    {
        $response = $this->post('http://127.0.0.1:8000/');
        $response->assertStatus(405);
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testStatusCodeProductsIndexOk()
    {
        $response = $this->get('http://127.0.0.1:8000/products/');
        $response->assertStatus(200);
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testStatusCodeProductsIndexError()
    {
        $response = $this->post('http://127.0.0.1:8000/products/');
        $response->assertStatus(404);
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testStatusCodeProductsStoreExcelError()
    {
        $response = $this->post('http://127.0.0.1:8000/products/');
        $response->assertStatus(404);
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testStatusCodeProductsShowOk()
    {
        $product_id = $this->getProductOk();
        $response = $this->get('http://127.0.0.1:8000/products/'.$product_id);
        $response->assertStatus(200);
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testStatusCodeProductsShowError()
    {
        $response = $this->get('http://127.0.0.1:8000/products/9999');
        $response->assertStatus(404);
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testStatusCodeProductsUpdateOk()
    {
        $product_id = $this->getProductOk();
        $response = $this->put('http://127.0.0.1:8000/products/'.$product_id.'?lm=1002&category=123123333&name=Furadeira AAA222&free_shipping=1&description=Furadeira super eficiente Y&price=140.00');
        $response->assertStatus(201);
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testStatusCodeProductsUpdateError()
    {
        $response = $this->put('http://127.0.0.1:8000/products/9999?lm=1002&category=123123333&name=Furadeira AAA222&free_shipping=1&description=Furadeira super eficiente Y&price=140.00');
        $response->assertStatus(404);
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testStatusCodeProductsDestroyOk()
    {
        $product_id = $this->getProductOk();
        $response = $this->delete('http://127.0.0.1:8000/products/'.$product_id);
        $response->assertStatus(200);
    }

    /**
     * Valid status code on uri
     *
     * @return void
     */
    public function testStatusCodeProductsDestroyError()
    {
        $response = $this->delete('http://127.0.0.1:8000/products/99999');
        $response->assertStatus(404);
    }

    /**
     * Insert data into the database.
     *
     * @return void
     */
    public function testInsertDataToDatabase()
    {
        DB::table('products')->insert([
            'lm'            => 1001,
            'name'          => 'Parafusadeira e Furadeira 3/8"',
            'category'      => 12123,
            'free_shipping' => 0,
            'description'   => 'Parafusadeira e Furadeira 3/8" 9,6V CD961-BR Bivolt Black & Decker',
            'price'         => '159.89',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);
        $this->assertDatabaseHas('products',[
            'lm'            => 1001,
            'name'          => 'Parafusadeira e Furadeira 3/8"',
            'category'      => 12123,
            'free_shipping' => 0,
            'description'   => 'Parafusadeira e Furadeira 3/8" 9,6V CD961-BR Bivolt Black & Decker',
            'price'         => '159.89'
        ]);

        DB::table('products')->insert([
            'lm'            => 1002,
            'name'          => 'Furadeira de Bancada 368W ',
            'category'      => 12123,
            'free_shipping' => 0,
            'description'   => 'Furadeira de Bancada 368W 1/2 HP FBM-160T 220V (380V) Motomil',
            'price'         => '1372.90',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);
        $this->assertDatabaseHas('products',[
            'lm'            => 1002,
            'name'          => 'Furadeira de Bancada 368W ',
            'category'      => 12123,
            'free_shipping' => 0,
            'description'   => 'Furadeira de Bancada 368W 1/2 HP FBM-160T 220V (380V) Motomil',
            'price'         => '1372.90'
        ]);

        DB::table('products')->insert([
            'lm'            => 1003,
            'name'          => 'Furadeira de Impacto 1/2" 650W',
            'category'      => 12123,
            'free_shipping' => 1,
            'description'   => 'Furadeira de Impacto 1/2" 650W PC650ID 127V (110V) Dexter IV',
            'price'         => '159.90',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);
        $this->assertDatabaseHas('products',[
            'lm'            => 1003,
            'name'          => 'Furadeira de Impacto 1/2" 650W',
            'category'      => 12123,
            'free_shipping' => 1,
            'description'   => 'Furadeira de Impacto 1/2" 650W PC650ID 127V (110V) Dexter IV',
            'price'         => '159.90'
        ]);

        DB::table('products')->insert([
            'lm'            => 1004,
            'name'          => 'Parafusadeira para Gesso 1/4',
            'category'      => 12123,
            'free_shipping' => 0,
            'description'   => 'Parafusadeira para Gesso 1/4" PC520PD 127V (110V) Dexter',
            'price'         => '219.90',
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);
        $this->assertDatabaseHas('products',[
            'lm'            => 1004,
            'name'          => 'Parafusadeira para Gesso 1/4',
            'category'      => 12123,
            'free_shipping' => 0,
            'description'   => 'Parafusadeira para Gesso 1/4" PC520PD 127V (110V) Dexter',
            'price'         => '219.90'
        ]);
    }
}